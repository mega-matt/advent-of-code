(ns advent.helpers)

(defmacro l [& body]
  `(fn [[[~'x1 ~'y1] [~'x2 ~'y2]]]
     (let [~'p1 [~'x1 ~'y1]
           ~'p2 [~'x2 ~'y2]]
       ~@body)))

(defn transpose [m]
  (apply mapv vector m))

(defmacro mmap [fn col]
  `(map (fn [v1#] (map ~fn v1#)) ~col))



(defmacro defxf
  "creates a simple reducable macro with let bindings"
  [name bindings var & body]
  `(defn ~name []
     (fn [xf#]
       (let
        ~bindings
         (fn
           ([] (xf#))
           ([res#] (xf# res#))
           ([res# ~var] (let [out# ((fn [] ~@body))]
                          (if (= :noop out#)
                            res#
                            (xf# res# out#)))))))))
