(ns advent.2021.day04)

(def calls (list 7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1))
(def puzzle (list 25,8,32,53,22,94,55,80,33,4,63,14,60,95,31,89,30,5,47,66,84,70,17,74,99,82,21,35,64,2,76,9,90,56,78,28,51,86,49,98,29,96,23,58,52,75,41,50,13,72,92,83,62,37,18,11,34,71,91,85,27,12,24,73,7,77,10,93,15,61,3,46,16,97,1,57,65,40,0,48,69,6,20,68,19,45,42,79,88,44,26,38,36,54,81,59,43,87,39,67))
(def calls puzzle)
(nth calls 11)
(def boards puzzle-boards)

(def boards (list 
22 13 17 11  0
8  2 23  4 24
21  9 14 16  7
6 10  3 18  5
1 12 20 15 19

3  15  0  2 22
9  18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
2  0 12  3  7))

(defn transpose [m]
  (apply mapv vector m))

(transpose (list 
            '(22 13 17 11 0)
            '(8 2 23 4 24)
            '(21 9 14 16 7)
            '(6 10 3 18 5)
            '(1 12 20 15 19)))
(defmacro mmap [fn col]
  `(map (fn [v1#] (map ~fn v1#)) ~col))


(defn solve2 [raw-board calls]
  (letfn [(board-grid [b] (partition 5 (partition 5 b)))
          (call-idx   [n] (first (keep-indexed #(when (= n %2) %1) calls)))
          (max-idx    [ns] (apply max (map call-idx ns)))
          (calc-score [bingo-idx uncalled] (* (apply + uncalled) (nth calls bingo-idx)))
          (bingo-idx  [board] (->> (concat board (transpose board)) ;; get vertical rows
                                   (map (juxt identity max-idx))    ;; [row max-call-index]
                                   (apply min-key second)           ;; [find best solve row]
                                   last))
          (uncalled   [board] (filter #(> (call-idx %) bingo-idx) (flatten board)))]
    
    (->> raw-board
         board-grid
         (sort-by bingo-idx)
         (#(calc-score bingo-idx (uncalled bingo-idx %))))
    #_(map #(let [bingo-idx (bingo-idx %)]
            [bingo-idx
             (calc-score bingo-idx (uncalled bingo-idx %))])
         (board-grid raw-board))))

(let [scores (solve2 puzzle-boards puzzle)]
  {
   :min (first scores)
   :max (last scores)})

(defn solve [raw-boards calls]
  ;; helper fns
  (let [as-grid         #(partition 5 (partition 5 %))
        with-verts      #(concat % (transpose %))
        call-idx        (fn [item] (first (keep-indexed #(when (= item %2) %1) calls)))
        max-idx         (comp (partial apply max)
                              (partial map call-idx))
        bingo-idx       (memoize #(apply min (map max-idx (with-verts %))))
        uncalled        (fn [board] (filter #(> (call-idx %) (bingo-idx board)) (flatten board)))
        calc-score      (fn [board] (* (apply + (uncalled board)) (nth calls (bingo-idx board))))]

    (->> raw-boards
         as-grid
         (sort-by bingo-idx)
         (map calc-score))))

((juxt first last) (solve puzzle-boards puzzle))

