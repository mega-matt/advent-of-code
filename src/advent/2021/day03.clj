(ns advent.2021.day03
  (:require [clojure.string :as str]))

(def puzzle-example
  (re-seq #"\d+"
"00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010"))

(defn transpose [m]
  (apply mapv vector m))

;; 01010
(def bin->int #(Integer/parseInt % 2))
;; star 1
(->> puzzle-example
     transpose
     (map frequencies)
     (map (juxt
           ;; gamma 
           (partial apply max-key second)
           ;: epsilon
           (partial apply min-key second)))
     (map keys) ;; drop freq counts
     transpose  ;; back to original row order
     (map (partial str/join ""))
     (map bin->int)
     (apply *))

(defn- most-common
  "tie goes to 1"
  [col] (->> col frequencies sort (apply max-key val) first))

(defn- least-common
  "tie goes to 0"
  [col] (->> col frequencies sort reverse (apply min-key val) first))
(defn- row-matches? [idx digit row]
  (= digit (nth row idx)))

(defn- solver
  ([comparator remaining] (solver comparator remaining 0))
  ([comparator remaining idx]
   (if (= 1 (count remaining))
     (Integer/parseInt (first remaining) 2)
     (let [common-n (comparator (-> remaining transpose (nth idx)))
           row-matches? (partial row-matches? idx common-n)]
       (recur comparator 
              (filter row-matches? remaining) 
              (inc idx))))))

(comment
  []
  (* (solver least-common puzzle-example)
     (solver most-common puzzle-example)))