(ns advent.2021.day10
  (:require [clojure.string :as str]
            [clojure.set :refer [map-invert]]))

(def example
"5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526")
;; my input
(def example "7222221271
6463754232
3373484684
4674461265
1187834788
1175316351
8211411846
4657828333
5286325337
5771324832")

(defn ->int [n]  (Integer/parseInt (str n)))

;; approach
;; increment with 9 -> f for flash
;; recur over grid replacing f -> z and increment adjacent

(let [row-len (str/index-of example "\n")
      puzzle (str/replace example "\n" "")]
  (letfn [(same-row? [n n2] (= (Math/floor (/ n 10)) (Math/floor (/ n2 10))))
          (in-puzzle? [i] (and (nat-int? i) (< i (count puzzle))))
          (get-adjacent-indexes
            [i]
            (filter (every-pred in-puzzle? (partial not= i))
                    (for [x (filter (partial same-row? i) [(dec i) i (inc i)])
                          y [-10 0 10]]
                      (+ x y))))
          (update-idx [str idx fun] (str/join "" [(subs str 0 idx) (fun (nth str idx)) (subs str (inc idx))]))
          (+pow [grid idx] (update-idx grid
                                       idx
                                       #(case %
                                          \z \z
                                          (\f \9) \f
                                          (inc (->int %)))))
          (apply-flash
            [grid]
            (if-let [idx (str/index-of grid "f")]
              (let [adjacent (get-adjacent-indexes idx)
                    new-grid (update-idx grid idx (constantly "z"))]
                (recur (reduce +pow new-grid adjacent)))
              grid))]

    ;; star 1
    (->> puzzle
         (iterate (comp apply-flash #(reduce +pow % (map-indexed (comp first vector) %)) #(str/replace % #"z" "0")))
         (take 101)
         (map (comp count (partial re-seq #"z")))
         (apply +)
         prn)

    ;; star 2
    (->> puzzle
         (iterate (comp apply-flash #(reduce +pow % (map-indexed (comp first vector) %)) #(str/replace % #"z" "0")))
         (take 1000)
         (keep-indexed (fn [i grid] (when (= grid (str/join "" (repeat 100 "z"))) i)))
         (first))
    ))

