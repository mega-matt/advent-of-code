(ns advent.2021.day01)
;; Day 1
(def puzzle-input (list 199
200
208
210
200
207
240
269
260
263))
(defn star1 [input]
  (->> input
       (partition 2 1)
       (filter #(apply < %))))
(count (star1 puzzle-input))

(defn star2 [input]
  (->> input
       (partition 3 1)
       (map #(apply + %))
       (partition 2 1)
       (filter #(apply < %))))

(count (star2 puzzle-input))

