(ns advent.2021.day05
  (:require [clojure.string :as clj-str]
            [advent.helpers :refer [l transpose]]))

(def example 
  "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2")

(defn abs [n] (max n (- n)))
(defn transpose [m]
  (apply mapv vector m))

(let [s (atom {})
      diagonal? (fn [x1 y1 x2 y2] (= (abs (- x1 x2)) (abs (- y1 y2))))
      horizontal? (fn [x1 y1 x2 y2] (or (= x1 x2) (= y1 y2)))
      keep? (fn [& args] (or  (apply horizontal? args) (apply diagonal? args)))
      parsed (read-string (str "(" (clj-str/replace example "->" "") ")"))
      lines (partition 2 (partition 2 parsed))
      on-line? (fn [x1 y1 x2 y2 x3 y3]
                 (zero? (- (* (- x3 x1) (- y2 y1)) (* (- y3 y1) (- x2 x1)))))]
  (loop [[x1 y1 x2 y2 & rest] parsed]
    (when (keep? x1 y1 x2 y2)
      (doseq [x (range (min x1 x2) (inc (max x1 x2)))
              y (range (min y1 y2) (inc (max y1 y2)))]
        (when (on-line? x1 y1 x2 y2 x y) (swap! s update [x y] (fnil inc 0)))))
    (when (seq rest) (recur rest)))
  (count (filter #(> % 1) (vals @s))))