(ns advent.2021.day12
  (:require [clojure.string :as str]))

(def example "start-A
start-b
A-c
A-b
b-d
A-end
b-end")

(def puzzle-map
  (->> (split-split example #"\n" #"-")
       (map reverse)
       (concat (split-split example #"\n" #"-"))
                        ;;  keep start in start and end in end pos
       (remove #(or (= "start" (second %))
                    (= "end" (first %))))
       (group-by first)
       (reduce-kv (fn [c i v] (assoc c i (map second v))) {})))

(defn split-split [s m m2] (map #(str/split % m2) (str/split s m)))
(defn uc? [s] (when (and (not (nil? s))
                         (= s (str/upper-case s)))
                s))
(def lc? (complement uc?))

;; first approach ~ 900 ms 
;; probably about to blow the stack though
(time (let [double? #(:has-double (meta %))
            can-use-node? (fn
                            [chain node]
                            (or (uc? node)
                                (not (double? chain))
                                (not-any? #{node} chain)))]
  ((fn my-fn [chain]
     (let [l (last chain)
           chain (with-meta chain
                   {:has-double (boolean (or (double? chain)
                                             (and (lc? l) (some #{l} (butlast chain)))))})
           next (puzzle-map l)
           can-next (filter (partial can-use-node? chain) next)]
       (if (= "end" l)
         1
         (apply + (map #(my-fn (conj chain %)) can-next))))) ["start"])))

;; second approach ~ 2.5 seconds (slower but safer with stack)
  
(time (let [has-double? (comp :double meta)
            needs-double? (complement has-double?)
            can-use-node? (fn [chain node]
                            (or (uc? node)
                                (needs-double? chain)
                                (not-any? #{node} chain)))
            add-item (fn [chain item]
                       (with-meta (conj chain item)
                         {:double (boolean
                                   (or (has-double? chain)
                                       (and (lc? item)
                                            (some #{item} chain))))}))
            score-paths (fn my-fn [chain]
                          (lazy-seq
                           (if
                            (= "end" (last chain))
                             (list 1)
                             (when-let [usable (seq (filter #(can-use-node? chain %)
                                                            (puzzle-map (last chain))))]
                               (flatten (map #(my-fn (add-item chain %)) usable))))))]
  (apply + (score-paths ["start"]))))

