(ns advent.2021.day02
  (:require [clojure.string :as str]))


(def puzzle-example "forward 5
down 5
forward 8
up 3
down 8
forward 2")

(def plus (fnil + 0))
(def minus (fnil - 0))
(defn split-split [s m m2] (map #(str/split % m2) (str/split s m)))
(def ->int #(Integer/parseInt %))

;; star 1
(defn- move [placement [action num]]
  (prn placement [action num])
  (case action
    "forward" (update placement :horizontal plus (->int num))
    "down"    (update placement :depth plus (->int num))
    "up"      (update placement :depth minus (->int num))))

(reduce move {} (split-split puzzle-example #"\n" #" "))

;; star 2
(defn- act [{:as placement h :horizontal d :depth a :aim} [action num]]
  (case action
    "down"    (update placement :aim plus (->int num))
    "up"      (update placement :aim minus (->int num))
    "forward" (assoc placement
                     :depth (plus d (plus a (->int num)))
                     :horizontal (plus h (->int num)))))

(reduce act {} (split-split puzzle-example #"\n" #" "))

