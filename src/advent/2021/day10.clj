(ns advent.2021.day10
  (:require [clojure.string :as str]
            [clojure.set :refer [map-invert]]))

(def example "[({(<(())[]>[[{[]{<()>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]")


(def adjacent-pair #"\(\)|\[\]|\{\}|\<\>")
(def match-close #"(\)|\]|\}|\>)")
(def get-open {\} \{, \) \(, \] \[, \> \<})
(def get-close (map-invert get-open))
(def bad-char-point
  {")" 3
   "]" 57
   "}" 1197
   ">" 25137})

(defn remove-pairs 
  "replace first adjacent pair until no more"
  [start]
  (if-let [match (first (re-seq adjacent-pair start))]
    (recur (str/replace start match ""))
    start))

(let [bad? (comp ffirst (partial re-seq match-close))]
 (->> (str/split example #"\n")
      (map (comp bad? remove-pairs))
      (remove nil?)
      (map bad-char-point)
      (apply +)))



(let [bad?            (comp ffirst (partial re-seq match-close))
      incomplete?     (comp not seq bad?)
      completion-str  (comp  (partial map get-close) reverse)
      char-point      {\) 1, \] 2, \} 3, \> 4}
      score           #(reduce (fn [c n] (+ (* c 5) n))
                               0
                               (map char-point %))
      middle #(nth % (/ (count %) 2))]
  (->> (str/split example #"\n")
       (map remove-pairs)
       (filter incomplete?)
       (map completion-str)
       (map score)
       sort
       middle))
