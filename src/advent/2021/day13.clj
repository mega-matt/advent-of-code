(ns advent.2021.day13
  (:require [clojure.string :as str]))

(defn split-split [s m m2] (map #(str/split % m2) (str/split s m)))
(defn ->int [n]  (Integer/parseInt (str n)))
(defmacro mmap [fn col]
  `(map (fn [v1#] (map ~fn v1#)) ~col))

(defn set-idx [c s idx] (str/join "" [(subs s 0 idx) c (subs s (inc idx))]))

(defn p->idx [max-x [x y]] (int (+ x (* y max-x))))
(def sum-nat #(/ (* % (inc %)) 2))

(defn output [points]
  (let [rows (+ 1 (second (apply max-key second points)))
        cols (+ 1 (first (apply max-key first points)))
        box-str (apply str (repeat (* rows cols) " "))
        raw-box (reduce (partial set-idx "▒") box-str (map (partial p->idx cols) points))]
    (run! #(prn (str/join "" %))
          (partition cols raw-box))))

(defn solve [point-str fold-str]
  (let [points (mmap ->int (split-split point-str #"\n" #","))
        apply-fold (fn [p over-number] (- p (max 0 (* 2 (- p (->int over-number))))))
        ;; map each fold to a function that applies the fold to a point
        folds (->> (re-seq #"(x|y)=(\d+)" fold-str)
                   (map (fn [[_ axis n]]
                          (fn [[x y]] (if (= "x" axis)
                                        [(apply-fold x n) y]
                                        [x (apply-fold y n)])))))]

    (reduce #(map %2 %1) points folds)))

(comment 
  []
  (def puzzle "6,10\n0,14\n9,10\n0,3\n10,4\n4,11\n6,0\n6,12\n4,1\n0,13\n10,12\n3,4\n3,0\n8,4\n1,10\n2,14\n8,10\n9,0")
  (def fold-pattern "fold along y=7\nfold along x=5")
  (output (solve puzzle fold-pattern)))