(ns advent.2021.day09
  (:require [clojure.set :refer [subset?  map-invert]]
            [clojure.string :as str]))


(def example "2199943210
3987894921
9856789892
8767896789
9899965678")

;; sum local lows 
(defn ->int [n]  (Integer/parseInt (str n)))
(let [row-len (str/index-of example "\n")
      puzzle (str/replace example "\n" "")]
  (letfn [(row [i] (int (Math/floor (/ i row-len))))
          (in-puzzle? [i] (and (nat-int? i) (< i (count puzzle))))
          (number-at [i] (when (and (not (neg? i)) (< i (count puzzle))) (->int (nth puzzle i))))
          (get-adjacent-indexes [i] (cond-> (list)
                                          (= (row i) (row (inc i))) (conj (inc i))
                                          (= (row i) (row (dec i))) (conj (dec i))
                                          (in-puzzle? (+ i row-len)) (conj (+ i row-len))
                                          (in-puzzle? (- i row-len)) (conj (- i row-len))))
          (keep-low [i adj] (when (< (number-at i) (apply min (map number-at adj))) adj))]
    (->> puzzle
         (map-indexed (comp first vector))
         (map get-adjacent-indexes)
         (map-indexed keep-low)
         (map-indexed #(when %2 (number-at %1)))
         (remove nil?)
         (map inc)
         (reduce + 0))))

(defn ->int [n]  (Integer/parseInt (str n)))
(time (let [row-len (str/index-of example "\n")
            puzzle (str/replace example "\n" "")]
  (letfn [(row [i] (int (Math/floor (/ i row-len))))
          (in-puzzle? [i] (and (nat-int? i) (< i (count puzzle))))
          (number-at [i] (when (in-puzzle? i) (->int (nth puzzle i))))
          (is-basin? [i1 i2] (let [n1 (number-at i1)
                                   n2 (number-at i2)]
                               (and (not= 9 n2) (> n2 n1))))
          (get-adjacent-indexes [i] (cond-> (list)
                                      (= (row i) (row (inc i))) (conj (inc i))
                                      (= (row i) (row (dec i))) (conj (dec i))
                                      (in-puzzle? (+ i row-len)) (conj (+ i row-len))
                                      (in-puzzle? (- i row-len)) (conj (- i row-len))))
          (is-local-low? [i adj] (< (number-at i) (apply min (map number-at adj))))
          (get-basins [i] (into #{i} (->> i
                                          get-adjacent-indexes
                                          (filter #(is-basin? i %))
                                          (mapcat get-basins))))]
    (->> puzzle
        ;;  get local lows
         (keep-indexed
          (fn [i _]
            (when (is-local-low? i (get-adjacent-indexes i)) i)))

        ;;  turn them into basins
         (pmap (comp count get-basins))

        ;;  pick best 3 and multiply
         sort
         (take-last 3)
         (apply *)))))